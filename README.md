# Project Title

The idea of this task is to create claculator, which takes commands in natural language and gives the
result as number. 

## Getting Started

Take a git clone on project and take it to your computer. Run on command line in the project folder "g++ Char.cpp -o main" and then when it should comile run ./main in the same folder. Program starts.
### Prerequisites

You need a windows or linux computer, in which you can build the project. G++ compiler is also needed.

## Usage

You can write commands like "ten over two". The numbers you can use are from 0 to 20. Operations are
plus, minus, times, over. 

# About the project

This was made by Ilona Skarp for Noroff and Experis Academy Finland.
